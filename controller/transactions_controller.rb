class TransactionsController < ApplicationController
  before_action :authenticate_user!, only: [:upgrade_plan, :upgrade_subdomain]
  
  ##--
  ## Created On: 26/07/2016
  ## Purpose: To show the subscription summary page the user has selected and brantree.
  ##++
  def subdomain_plan
    return if redirect_to_correct_subdomain
    gon.client_token = generate_client_token
    session[:subdomain_type_id] = session[:plan_id] = session[:setup_plan_id] = session[:amount] = nil
    begin
      session[:back_url] = request.url
      params[:plan_id] = decrypt(params[:plan_id])
      params[:subdomain_type_id] = decrypt(params[:subdomain_type_id])
      plan = Plan.find(params[:plan_id])
      render_404 and return unless plan.is_ocm?
      subdomain_type = SubdomainType.find(params[:subdomain_type_id])
      session[:subdomain_type_id] = params[:subdomain_type_id]
      session[:plan_id] = plan.id
      @plan_amount = plan.plan_amount
      setup_fee_require = Subscription.joins(:plans).where("subscriptions.subdomain_user_id = ? AND plans.plan_type_id = ?", User.find_by(username: current_subdomain.subdomain_name).subdomain_user.id, PlanType.find_by(type_name: "SETUP")).empty?
      if subdomain_type.has_setup_plan? && setup_fee_require
        setup_plan = plan.subdomain_types.find_by(subdomain_type: subdomain_type.subdomain_type).setup_plan
        @setup_fee = setup_plan.plan_amount
        session[:setup_plan_id] = setup_plan.id
      end
      @amount = @plan_amount + (@setup_fee.nil? ? 0 : @setup_fee)
      session[:amount] = @amount
      session[:redirect_to] = my_account_path
      @plan_amount = plan.plan_amount
      if session[:new_user]["role"] != "CUSTOMER"
        setup_plan = plan.subdomain_types.find_by(subdomain_type: session[:new_user]["role"]).setup_plan
        @setup_fee = setup_plan.plan_amount unless setup_plan.nil?
      else
        @addons = Plan.find(session[:addons].split(','))
      end
      session[:amount] = @amount = @plan_amount + (@setup_fee.nil? ? 0 : @setup_fee) + (@addons.present? ? @addons.map(&:plan_amount).inject(:+) : 0)
      session[:redirect_to] = my_account_path
    rescue Exception => e 
      #render_404
    end
    
  end


  ##--
  ## Created On: 03/12/2014
  ## Re-write On: 18/12/2014
  ## Purpose: To show the subscription summary page the user has selected.
  ##++
  def subscription_details
    return if redirect_to_correct_subdomain
    plan = Plan.find(session[:plan_id])
    @plan_amount = plan.plan_amount
    if session[:new_user]["role"] != "CUSTOMER"
      setup_plan = plan.subdomain_types.find_by(subdomain_type: session[:new_user]["role"]).setup_plan
      @setup_fee = setup_plan.plan_amount unless setup_plan.nil?
    else
      @addons = Plan.find(session[:addons].split(','))
    end
    session[:amount] = @amount = @plan_amount + (@setup_fee.nil? ? 0 : @setup_fee) + (@addons.present? ? @addons.map(&:plan_amount).inject(:+) : 0)
  end

  ##--
  ## Created On: 12/12/2014
  ## Purpose: To show the subscription summary page.
  ##++
  def upgrade_plan
    return if redirect_to_correct_subdomain
    begin
      params[:plan_id] = decrypt(params[:plan_id])
      plan = Plan.find(params[:plan_id])
      render_404 and return if plan.is_ocm?
      if plan.type_is?("RENEW")
        @plan_amount = plan.plan_amount
      else
        @addons = [plan]
      end
      session[:plan_id] = nil
      session[:plan_id] = params[:plan_id]
      @amount = (@plan_amount.nil? ? 0 : @plan_amount) + (@addons.nil? ? 0 : @addons.map(&:plan_amount).inject(:+))
      session[:amount] = nil
      session[:amount] = @amount
      if params[:uploaded_media_id].blank?
        session[:redirect_to] = "/customer_profile"
      else
        session[:redirect_to] = "/media_details/#{params[:uploaded_media_id]}"
      end
      render 'subscription_details'
    rescue Exception => e 
      render_404
    end
  end

  ##--
  ## Created On: 12/12/2014
  ## Purpose: To show the subscription summary page.
  ##++
  def upgrade_subdomain
    return if redirect_to_correct_subdomain
    session[:subdomain_type_id] = session[:plan_id] = session[:setup_plan_id] = session[:amount] = nil
    begin
      params[:plan_id] = decrypt(params[:plan_id])
      params[:subdomain_type_id] = decrypt(params[:subdomain_type_id])
      plan = Plan.find(params[:plan_id])
      render_404 and return unless plan.is_ocm?
      subdomain_type = SubdomainType.find(params[:subdomain_type_id])
      session[:subdomain_type_id] = params[:subdomain_type_id]
      session[:plan_id] = plan.id
      @plan_amount = plan.plan_amount
      setup_fee_require = Subscription.joins(:plans).where("subscriptions.subdomain_user_id = ? AND plans.plan_type_id = ?", User.find_by(username: current_subdomain.subdomain_name).subdomain_user.id, PlanType.find_by(type_name: "SETUP")).empty?
      if subdomain_type.has_setup_plan? && setup_fee_require
        setup_plan = plan.subdomain_types.find_by(subdomain_type: subdomain_type.subdomain_type).setup_plan
        @setup_fee = setup_plan.plan_amount
        session[:setup_plan_id] = setup_plan.id
      end
      @amount = @plan_amount + (@setup_fee.nil? ? 0 : @setup_fee)
      session[:amount] = @amount
      session[:redirect_to] = my_account_path
      render 'subscription_details'
    rescue Exception => e 
      render_404
    end
  end

  ##--
  ## Created On: 02/12/2014
  ## Purpose: To initiate transaction.
  #++
  def new
    return if redirect_to_correct_subdomain
    gon.client_token = generate_client_token
  end

  ##--
  ## Created On: 02/12/2014
  ## Modified on: 13/12/2014
  ## Purpose: To create transaction using Braintree Payment gateway.
  #++
  def create
    @result = pay(session[:amount], params[:payment_method_nonce])
    if @result.success?
      if !session[:redirect_to].blank? && !session[:plan_id].blank? && !current_user.nil?
        plan = Plan.find(session[:plan_id])
        if plan.is_ocm?
          subdomain_plan_change(plan, SubdomainType.find(session[:subdomain_type_id]), @result, User.find_by(username: current_subdomain.subdomain_name))
        else
          customer_plan_change(plan, @result)
        end
        redirect = session[:redirect_to]
        session[:redirect_to] = session[:plan_id] = nil
        unless session[:back_url]!=nil && session[:back_url].include?("subdomain_plan")
          flash[:message] = "Transaction Successful"
          flash[:type] = :success
        end
        redirect_to redirect and return 
      end
      transaction = Transaction.create(amount: @result.transaction.amount.to_f, status: true, payment_method: @result.payment_method, created_at: @result.transaction.created_at)
      session[:transaction] = transaction.id
      redirect_to :controller => "users/registrations", :action => "create", :status => 307
    else
      redirect_to home_path, flash:{title: "Payment Unsuccessful", message: "Something went wrong while processing your transaction. Please try again!", type: :error}
    end
  end

  private

  ##--
  ## Modified On: 14/12/2014
  ## Purpose: To indent code.
  #++
  def pay(amount, payment_method)
    Braintree::Transaction.sale(amount: amount, payment_method_nonce: payment_method)
  end

  ##--
  ## Created On: 14/12/2014
  ## Purpose: To change plan for customer
  #++
  def customer_plan_change(plan, result)
    subscription = nil
    if plan.type_is?("RENEW")
      subscription = current_user.subdomain_user.subscriptions.create(effective_from: DateTime.now, effective_to: 30.days.from_now, is_recurring: false)
    else
      subscription = current_user.subdomain_user.subscriptions.order(:created_at).last
    end
    subscription.plan_subscriptions.create(plan: plan)
    transaction = Transaction.create(user: current_user, subscription: subscription, amount: result.transaction.amount.to_f, status: true, payment_method: result.payment_method, created_at: result.transaction.created_at)
    transaction.transaction_details.create(plan: plan, uploaded_media_id: nil, detail_amount: plan.plan_amount)
  end

  ##--
  ## Created On: 14/12/2014
  ## Purpose: To change plan for subdomain
  #++
  def subdomain_plan_change(plan, subdomain_type, result, user)
    subscription = user.subdomain_user.subscriptions.create(effective_from: DateTime.now, effective_to: 30.days.from_now, is_recurring: false)
    subscription.plan_subscriptions.create(plan: plan)
    subscription.plan_subscriptions.create(plan: Plan.find_by(id: session[:setup_plan_id])) if session[:setup_plan_id]
    transaction = Transaction.create(user: user, subscription: subscription, amount: result.transaction.amount.to_f, status: true, payment_method: result.payment_method, created_at: result.transaction.created_at)
    transaction.update(user: user, subscription: subscription)
    transaction.transaction_details.create(plan: plan, uploaded_media_id: nil, detail_amount: plan.plan_amount)
    user.subdomain.update(subdomain_type_name: subdomain_type.subdomain_type)
    User.find_by(username: user.subdomain.subdomain_name).update(deactive: false) if !user.subdomain.is_active?  
    session[:payment_status]= transaction.status
  end

  ##--
  ## Created On: 02/12/2014
  ## Purpose: To generate Client token.
  #++
  def generate_client_token
    Braintree::ClientToken.generate
  end
end
