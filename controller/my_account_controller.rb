class MyAccountController < ApplicationController
  before_action :authenticate_user!
  before_action :redirect_to_myaccount
  before_action :restrict_non_tenant_owner, only: [:exchange_list, :create_exchange, :destroy_exchange, :affiliation_list, :create_affiliation, :delete_affiliation, :affiliation_approval_list, :affiliation_approval, :affiliation_sorting]
  skip_before_filter :verify_authenticity_token, only: [:cancel_account, :update_church_account_info, :apply_custom_theme, :apply_default_theme, :promotion_url, :registered_user]

  ##--
  ## Created On: 12/12/2014
  ## Modified On: 15/12/2014
  ## Purpose: To show my account page.
  ##++
  def my_account_page
  
    @subdomain = current_subdomain
    theme = Theme.where(subdomain_id: @subdomain.id)
    @user = User.find_by(username: @subdomain.subdomain_name)
    @can_update = can_update_church_info
    return if redirect_to_correct_subdomain
    return if redirect_to_myaccount
    if !current_subdomain.is_ocm?
      @subdomain_types = SubdomainType.includes(:plans).where.not(subdomain_type: "OCM")
      @current_plan = @user.current_plan
      @setup_fee_require = Subscription.joins(:plans).where("subscriptions.subdomain_user_id = ? AND plans.plan_type_id = ?", @user.subdomain_user.id, PlanType.find_by(type_name: "SETUP")).empty?
    end
    @show = true
  end

  ##--
  ## Created On: 1/05/2016
  ## Purpose: To Initalize a submerchant account model
  def submerchant_account_tab
    @submerchant_account = current_user.submerchant_accounts || SubmerchantAccounts.new
  end

  ##--
  ## Created On: 21/06/2016
  ## Purpose: To Create a submerchant account for current subdoamin in braintree.
  ##++
  def submerchant_account_create
    merchant_account_params = {
                              :individual => {
                                :first_name => params["submerchant_accounts"]["first_name"],
                                :last_name => params["submerchant_accounts"]["last_name"],
                                :email => current_user.email,
                                :phone =>  params["submerchant_accounts"]["phone"],
                                :date_of_birth => params["submerchant_accounts"]["date_of_birth"].to_date,
                                :address => {
                                  :street_address => params["submerchant_accounts"]["street_address"],
                                  :locality =>  params["submerchant_accounts"]["locality"],
                                  :region => params["submerchant_accounts"]["region"],
                                  :postal_code =>  params["submerchant_accounts"]["postal_code"],
                                }
                              },
                                :business => {
                                  :legal_name => params["submerchant_accounts"]["legal_name"],
                                  :dba_name => params["submerchant_accounts"]["legal_name"],
                                  :tax_id =>params["submerchant_accounts"]["tax_id"],
                                  :address => {
                                    :street_address =>params["submerchant_accounts"]["street_address"],
                                    :locality => params["submerchant_accounts"]["locality"],
                                    :region => params["submerchant_accounts"]["region"],
                                    :postal_code => params["submerchant_accounts"]["postal_code"]
                                  }
                                },

                              :funding => {
                                :destination => Braintree::MerchantAccount::FundingDestination::Bank,
                                :email => current_user.email,
                                :account_number => params["submerchant_accounts"]["account_number"],
                                :routing_number => params["submerchant_accounts"]["routing_number"],
                              },
                              :tos_accepted => true,
                              :master_merchant_account_id => ENV['MASTER_MERCHANT_ACCOUNT_ID']
                            }
    submerchant_account = Braintree::MerchantAccount.create(merchant_account_params)
    if submerchant_account.success?
      submerchant = current_user.create_submerchant_accounts(submerchant_account_params)
      submerchant.update_attributes(:submerchant_id => submerchant_account.merchant_account.id)
      Subdomain.update_or_create_milestone(current_subdomain.id,Subdomain.milestone_for_payment_details)
      redirect_to :back
    else
      render json: {errors: submerchant_account.errors.collect{ |e| e.message }}
    end 
  end

  ##--
  ## Created On: 12/12/2014
  ## Modified On: 15/12/2014
  ## Purpose: To render different partial on click of different tabs.
  ##++
  def switch_tab
    @selected_tab = params["selected_tab"]
    if params["selected_tab"] == "brand_new_messages" && !params[:page].blank?
      if params[:order_by].present? && params[:order]
        @uploaded_medias = UploadedMedium.in_subdomain(current_subdomain).order("#{params[:order_by]} #{params[:order]}").paginate(:per_page => 20, :page => params[:page])
      else
        @uploaded_medias = UploadedMedium.in_subdomain(current_subdomain).desc.paginate(per_page: 20, page: params[:page])
      end
      render partial: "brand_new_messages" and return
    else
      @uploaded_medias = UploadedMedium.in_subdomain(current_subdomain).deactivated_media.desc.paginate(per_page: 20, page: params[:page])
    end
    if current_subdomain.is_ocm?
      @default_theme = Theme.where("id < #{(ENV['THEME_RESERVE'].to_i + 1)}").first(11)
    else
      @default_theme = Theme.where("id < #{(ENV['THEME_RESERVE'].to_i + 1)}").last(11)
    end
    @default_theme << current_theme if current_theme && current_theme.theme_name == "CUSTOM"
  end
  
  ##--
  ## Created On: 12/12/2014
  ## Purpose: To cancel account of subdomain and all its users.
  ## Updated On: 28/07/2016
  ## Purpose: To add reason field for feedback
  ##++
  def cancel_account
    user = User.find_by(username: current_subdomain.subdomain_name)
    admin_users = Subdomain.find_by(subdomain_name: current_subdomain.subdomain_name).subdomain_users.map(&:user_id)
    feedback = params["feedback"]["text"]
    reason = params["feedback"]["reason"]
    unless feedback.blank? 
      Feedback.create(user_id: user.id, feedback: feedback, feedback_type: "feedback_at_deactivation", reason: reason)
    end
    if User.where(id: user.id).update_all(cancelled_on: DateTime.now)
      admin_users.each do |admin|
        admin_user = User.find(admin)
        admin_user.update(cancelled_on: DateTime.now) if !admin_user.is_customer?
      end
      redirect_to "/survey"
    end
  end
  

  ##--
  ## Created On: 29/07/2016
  ## Purpose: To show survey page
  ##++
  def survey  
    @user = User.find_by(username: current_subdomain.subdomain_name)
    @feedback = @user.feedbacks.last
  end
  
  ##--
  ## Created On: 29/07/2016
  ## Updated On: 16/08/2016
  ## Purpose: To send mail for deactivation of account, to change the instance variable as local.
  ##++
  def feedback_survey
    user = User.find_by(username: current_subdomain.subdomain_name)
    feedback = user.feedbacks.last
    UserMailer.send_deactivation_details(user, feedback).deliver
    sign_out user
    redirect_to root_path, flash:{title: "Account cancelled", message: "Your account has been cancelled successfully", type: :success}
  end
  ##--
  ## Created On: 15/12/2014
  ## Purpose: To give role to user for administration
  #++
  def user_administration
    @user = User.new
  end
 
  ##--
  ## Created On: 15/12/2014
  ## Purpose: To create user according to their administration role.
  #++ and send mail for username and password 
  def create_administration
    current_subdomain_id = current_subdomain.id
    send_mail= params[:mail]
    user = User.new(name: params[:name], email: params[:email], username: params[:username], user_role_id: params[:user_role_id])
    user.password = params[:password]
    user.password_confirmation = params[:password_confirmation]
    if user.save
      SubdomainUser.create(subdomain_id: current_subdomain_id, user_id: user.id)  rescue nil
      if send_mail == "1"
        begin
          UserMailer.registration_confirmation(user, user.password).deliver 
        rescue => e
          puts "Unable to send email"
        end
        #UserMailer.registration_confirmation(user, user.password).deliver 
      end
      redirect_to my_account_path(user_admin: "yes"), flash:{title: "User created", message: "user created successfully", type: :success}
    else
      redirect_to my_account_path(user_admin: "yes"), flash:{title: "User created", message: "user created successfully", type: :success}
    end
  end
  
  ##--
  ## Created On: 20/12/2014
  ## Purpose: To list the registered admin users and apply search on it.
  #++
  def search_registered_user
    @registered_users = User.admin_users_of_subdomain(request.subdomain)
  end

  ##--
  ## Created On: 27/05/2015
  ## Purpose: To go back to admin_user form page.
  #++
  def registered_user
  end

  ##--
  ## Created On: 21/12/2014
  ## Purpose: To delete admin user.
  #++
  def delete_registered_user
    @registered_user = User.find( params["user_id"])
    @registered_user.destroy
    redirect_to :back, flash:{title: "User deleted", message: "User deleted successfully", type: :success}
  end

  ##--
  ## Created On: 15/12/2014
  ## Purpose: To check for duplicate username
  #++
  def check_duplicate_username
    @user=User.find_by_username(params[:username])
    respond_to do |format|
      format.json { render :json => !@user}
    end
  end

  ##--
  ## Created On: 15/12/2014
  ## Purpose: To check for duplicate email
  #++
  def check_duplicate_email
    @email=User.find_by_email(params[:email])
    respond_to do |format|
      format.json { render :json => !@email}
    end
  end

  ##--
  ## Created On: 27/04/2016
  ## Purpose: request for exchange
  def exchange_request
    @subdomain = Subdomain.find(current_subdomain.id).update(exchange_status: 1)
    Subdomain.update_or_create_milestone(current_subdomain.id, Subdomain.milestone_for_agree_to_toc) 
    flash[:message] = "You have requested for exchange successfully"
    flash[:type] = :success
    respond_to do |format|
      format.json { render :json => @subdomain}
    end
  end

  ##--
  ## Created On: 20/05/2016
  ## Purpose: To show the list of all Subdomains for Affilation.
  def affiliation_list
    if current_subdomain.is_church_musician?
      include_ids = Subdomain.where(subdomain_type_id: 3).map(&:id)
      include_ids.delete(current_subdomain.id)
      @subdomains = search_affiliation(params, include_ids)
    elsif current_subdomain.is_church?
      include_ids = Subdomain.where("subdomain_type_id=3 or subdomain_type_id= 4").map(&:id)
      include_ids.delete(current_subdomain.id)
      @subdomains = search_affiliation(params, include_ids)
    else
      include_ids = Subdomain.where("subdomain_type_id=1 or subdomain_type_id= 2").map(&:id)
      @subdomains = search_affiliation(params, include_ids)
    end
  end

  def search_affiliation(params, include_ids)
    if params[:search_affiliation].present?
      @subdomains = Subdomain.where("id in (?) and subdomain_name like ?", include_ids, '%'+params[:search_affiliation]+'%').order('created_at DESC').paginate(:per_page => 20, :page => params[:page])
    else
      @subdomains = Subdomain.where("id in (?)", include_ids).order('created_at DESC').paginate(:per_page => 20, :page => params[:page])
    end
  end

  ##--
  ## Created On: 02/05/2016
  ## Purpose: To create an affiliation with other subdomain.
  ## Updated On: 16/09/2016
  ## Purpose: To remove restriction of five affiliation
  #++
  def create_affiliation
    if params[:affiliator_id].present?
      affiliation = Affiliation.create(subdomain_id: current_subdomain.id, affiliator_id: params[:affiliator_id])
      flash[:message] = "Affiliation created successfully."
      flash[:type] = :success
      redirect_to affiliation_list_path
    else
      flash[:message] = "We cannot affiliate you with this [church] at this time. Please try another affiliation"
      flash[:type] = :error
      redirect_to affiliation_list_path
    end
  end

  def delete_affiliation
    affiliation = Affiliation.find(params[:id])
    affiliation.destroy
    flash[:notice] = "Affiliation removed successfully."
    redirect_to affiliation_list_path
  end

  def affiliation_approval_list
    @affiliations = Affiliation.where("affiliator_id in (?) and status=0", current_subdomain.id).order('created_at DESC').paginate(:per_page => 20, :page => params[:page])
  end

  ##--
  ## Created On: 02/05/2016
  ## Purpose: To approve or reject the affiliation request for subdomains.
  #++
  def affiliation_approval
    unless params[:approve_subdomain].blank?
      affiliatee_ids = params[:approve_subdomain]
      if params.include?("approve_affiliation_request")
        affilation = Affiliation.where("id in (?)", affiliatee_ids).update_all(status: 1)
        flash[:message] = "You have successfully approved the request"
        flash[:type] = :success
      else
        affilation = Affiliation.where("id in (?)", affiliatee_ids).update_all(status: 2)
        flash[:message] = "You have successfully rejected the request"
        flash[:type] = :success
      end
    else
      flash[:message] = "You have not selected any request."
      flash[:type] = :error
    end
    redirect_to '/affiliation_approval_list'
  end

  ##--
  ## Created On: 16/12/2014
  ## Purpose: To update church account information.
  ## Updated On: 24/06/2016
  ## Purpose: To Update title of user.
  ## Updated On: 18/07/2016
  ## Purpose: To Update last name of user.
  #++
  def update_church_account_info
    user = User.find_by(username: request.subdomain)
    user.update(name: params["church"]["name"], last_name: params["church"]["last_name"], email: params["church"]["email"],title_id:params["title_id"])
    user.address.update(street1: params["address"]["street1"],
     street2: params["address"]["street2"], city: params["address"]["city"], state: params["address"]["state"], postal_code: params["address"]["postal_code"], country:params["address"]["country"])
    unless params["church"]["password"].blank?
      user.update(password: params["church"]["password"])
    end
    current_subdomain.update(business_email: params[:subdomain_email]) if params[:subdomain_email].present?
    sign_in(user, :bypass => true)
    redirect_to my_account_path, flash:{title: "Update Successful", message: "Account information updated", type: :success}
  end

  ##--
  ## Created On: 23/12/2014
  ## Purpose: [Ajax] To apply predefined theme
  #++
  def apply_default_theme
    return if current_user.is_customer?
    favicon_path = Theme.find_by_subdomain_id(current_subdomain.id)
    if favicon_path.present? && params[:theme][:favicon_logo].present? && favicon_path.favicon_image_path.present?
      (FileUtils.rm_rf("#{Rails.root.to_s}/public/favicons/#{favicon_path.favicon_image_path}") if File.exists?("#{Rails.root.to_s}/public/favicons/#{favicon_path.favicon_image_path}")) 
    end
    theme = Theme.where("subdomain_id = ? and id > ?", current_subdomain.id, ENV['THEME_RESERVE'])
    logo_image_ext = File.extname(params[:theme][:logo_image_path].original_filename) unless params[:theme][:logo_image_path].blank?
    favicon_image_ext = File.extname(params[:theme][:favicon_logo].original_filename) unless params[:theme][:favicon_logo].blank?
    custom_theme = Theme.find_by(id: params[:theme_id]).dup
    params_hash = { 
      subdomain_id: current_subdomain.id
    }
    params_hash.merge!({theme_name: "CUSTOM"}) unless params[:theme][:header] == current_theme.header && params[:theme][:header_description] == current_theme.header_description && params[:theme][:logo_image_path].blank?

    params_hash.merge!({header: params[:theme][:header]}) unless params[:theme][:header] == current_theme.header

    params_hash.merge!({header_description: params[:theme][:header_description]}) unless params[:theme][:header_description] == current_theme.header_description
    # To store subdomain sucription check value
    subdomain = Subdomain.find(current_subdomain.id)
    subdomain.update_attributes(:subscription_check=>params[:subscription_check],:personal_url=>params[:theme][:personal_url])
    theme_logo = Theme.where("subdomain_id=?",current_subdomain.id).last
    if theme.present?
      custom_theme.id = theme.first.id
      theme.delete_all
    end
    custom_theme.update(params_hash)
    Subdomain.update_or_create_milestone(current_subdomain.id, Subdomain.milestone_for_custom_theme_user) 
    if !params[:theme][:favicon_logo].blank?
      timestamp = Time.now.strftime('%Y%m%d%H%M%S000')
      custom_theme.update(favicon_image_path: "#{custom_theme.id}#{timestamp}#{favicon_image_ext}")
      File.open(custom_theme.favicon_image_physical, "wb") { |f| f.write(params[:theme][:favicon_logo].read) }
      favicon = Theme.find_by_subdomain_id(current_subdomain.id)
      if favicon.present?
        if File.exists?("#{Rails.root.to_s}/public/subdomain/logo/#{favicon.favicon_image_path}")
          FileUtils.mv("#{Rails.root.to_s}/public/subdomain/logo/#{favicon.favicon_image_path}", "#{Rails.root.to_s}/public/favicons/") 
        end
      end
    end
    #render :action => "crop" if !params[:theme][:logo_image_path].blank?
    unless params[:theme][:logo_image_path].blank?
      custom_theme.update(logo_image_path: "#{custom_theme.id}#{logo_image_ext}")
      Subdomain.update_or_create_milestone(current_subdomain.id, Subdomain.milestone_for_customize_store)
      File.open(custom_theme.logo_image_physical, "wb") { |f| f.write(params[:theme][:logo_image_path].read) }
    end
    reset_theme
    @custom_theme = custom_theme  
    if params[:theme][:favicon_logo].blank?
      @custom_theme.update_attributes({favicon_image_path: favicon_path.favicon_image_path}) if favicon_path.present?
    end
    if theme_logo.present?
      @custom_theme.update_attributes({logo_image_path: theme_logo.logo_image_path}) if params[:theme][:logo_image_path].blank?
    end
    if !params[:theme][:logo_image_path].blank?
      set_cookies_for_theme
      cookies[:logo_path] = File.exist?(current_theme.logo_image_physical || "") ? custom_theme.logo_image_url : "/subdomain/logo/logo.png"
      render :action => "crop" 
    else
      redirect_to "/my_account#theme"
    end
  end
  
  ##--
  ## Created On: 27/04/2016
  ## Purpose: To Resize logo Image for Business user.
  #++
  def update_cropping_image
    if params[:commit] != "Don't Crop"
      custom_theme = Theme.find_by(id: params["theme"]["theme_id"])
      img = MiniMagick::Image.open(custom_theme.logo_image_physical)
      crop_string = params["theme"]["crop_w"]+"x"+params["theme"]["crop_h"]+"+"+params["theme"]["crop_x"]+"+"+params["theme"]["crop_y"]
      img.crop(crop_string)
      img.write custom_theme.logo_image_physical
    end 
    redirect_to "/my_account#theme" 
  end

  ##--
  ## Purpose:- To set color code value for subdomain user.
  ## Created On: 23/12/2014
  ## Purpose: [Ajax] To apply custom theme
  #++
  def apply_custom_theme
    return if current_user.is_customer?
    logo_image_ext = File.extname(params[:theme][:logo_image_path].original_filename) unless params[:theme][:logo_image_path].blank?
    background_image_ext = File.extname(params[:theme][:background_image_path].original_filename) unless params[:theme][:background_image_path].blank?   
    params_hash = { 
      theme_name: "CUSTOM", 
      background_color: params[:theme][:background_color], 
      subdomain_title_color: params[:theme][:subdomain_title_color], 
      header_color: params[:theme][:header_color], 
      footer_color: params[:theme][:footer_color], 
      subdomain_id: current_subdomain.id,
      header_font_color: params[:theme][:header_font_color],
      other_font_color: params[:theme][:other_font_color],
      gradient: params[:gradient].present? ? params[:gradient] : "0",
      #header: params[:theme][:header],
      #header_description: params[:theme][:header_description]
    }
    theme = Theme.where("subdomain_id = ? and id > ?", current_subdomain.id, ENV['THEME_RESERVE'])
    if theme.present?
      hsh = params_hash
      params_hash = hsh.delete_if { |k, v| v.blank? }
      theme.update_all(params_hash)
      theme = theme.first
    else
      theme = Theme.create(params_hash)
    end
    #unless params[:theme][:logo_image_path].blank?
      #theme.update(logo_image_path: "#{theme.id}#{logo_image_ext}")
      #File.open(theme.logo_image_physical, "wb") { |f| f.write(params[:theme][:logo_image_path].read) }
    #end
    
    unless params[:theme][:background_image_path].blank?
      theme.update(background_image_path: "#{theme.id}#{background_image_ext}")
      File.open(theme.background_image_physical, "wb") { |f| f.write(params[:theme][:background_image_path].read) }
    end
    
    @theme = theme
    session[:theme_updated] = true
    theme.touch
    reset_theme
  end

  ##--
  ## Created On: 12/05/2016
  ## Purpose: Provide ability to Remove background image when customizing theme.
  #++
  def remove_background_image
    return if current_user.is_customer?   
    theme = Theme.where("subdomain_id = ? and id > ?", current_subdomain.id, ENV['THEME_RESERVE'])
    if theme.present?
      theme.last.update(background_image_path: "")
    end
    @theme = theme
    session[:theme_updated] = true
    reset_theme
    redirect_to "/my_account#theme"
  end

  ##--
  ## Created On: 23/12/2014
  ## Purpose: [Ajax] To render custom theme view
  #++
  def render_theme
    theme = Theme.find(params[:id])
    render partial: "theme_management1", locals: { theme: theme }
  end

  def can_update_church_info
    return true if current_user && ["TENANT_OWNER","SUPER_ADMIN"].include?(current_user.user_role.role)
  end


  ##--
  ## Created On: 06/12/2014
  ## Purpose: [Method: GET][Serve to Ajax], To search messages(media) by different parameters
  #++
  def search_messages
    return if redirect_to_correct_subdomain
      params[:search_by] = params[:search_by].blank? ? 'all' : params[:search_by]
      params[:search_text] ||= ''
      params[:last] ||= ''
      params[:page] = 1 if params[:page].to_i.zero?
      search_columns = ["media_file_name", "duration", "created_at", "presenter_name"]
      if params[:order_by].present? && params[:order].present? && search_columns.include?(params[:order_by]) && ["ASC", "DESC"].include?(params[:order])
        @uploaded_media = media_search(params[:search_by], URI.unescape(params[:search_text]), params[:last], current_subdomain, params[:page], 20, params[:order_by], params[:order])
      else
        @uploaded_media = media_search(params[:search_by], URI.unescape(params[:search_text]), params[:last], current_subdomain, params[:page], 20)
      end
      params[:type] = nil
      render partial: 'brand_new_messages' if request.format == "text/html"
  end

  ##--
  ## Created On: 24/05/2016
  ## Purpose: [Method: GET][Serve to Ajax], To sort the affiliation list by subdomain name.
  #++
  def affiliation_sorting
    return if redirect_to_correct_subdomain
      params[:search_text] ||= ''
      if params[:order_by].present? && params[:order].present? && ["ASC", "DESC"].include?(params[:order])
        @subdomains = sorted_affiliation_list(params[:order_by], params[:order], params[:search_text])
      end
      render :partial => "affiliation_table" if request.format == "text/html"      
  end

  def sorted_affiliation_list(order_by, order, search_affiliation)
    if current_subdomain.is_organization?
      include_ids = Subdomain.where(subdomain_type_id: 1).map(&:id)
      include_ids.delete(current_subdomain.id)
      if search_affiliation.present?
        @subdomains = Subdomain.where("id in (?) and subdomain_name like ?", include_ids, '%'+search_affiliation+'%').order("#{order_by} #{order}").paginate(:per_page => 20, :page => params[:page])
      else
        @subdomains = Subdomain.where("id in (?)", include_ids).order("#{order_by} #{order}").paginate(:per_page => 20, :page => params[:page])
      end
    elsif current_subdomain.is_church?
      include_ids = Subdomain.where("subdomain_type_id=3 or subdomain_type_id= 4").map(&:id)
      include_ids.delete(current_subdomain.id)
      if search_affiliation.present?
        @subdomains = Subdomain.where("id in (?) and subdomain_name like ?", include_ids, '%'+search_affiliation+'%').order("#{order_by} #{order}").paginate(:per_page => 20, :page => params[:page])
      else
        @subdomains = Subdomain.where("id in (?)", include_ids).order("#{order_by} #{order}").paginate(:per_page => 20, :page => params[:page])
      end
    else
      exclude_ids = [Subdomain.first.id, current_subdomain.id]
      if search_affiliation.present?
        @subdomains = Subdomain.where("id not in (?) and subdomain_name like ?", exclude_ids, '%'+search_affiliation+'%').order("#{order_by} #{order}").paginate(:per_page => 20, :page => params[:page])
      else
        @subdomains = Subdomain.where("id not in (?)", exclude_ids).order("#{order_by} #{order}").paginate(:per_page => 20, :page => params[:page])
      end
    end
  end

  ##--
  ## Created On: 23/05/2015
  ## Purpose: To save the facebook and twitter url of subdomain.
  #++
  def promotion_url
    unless !(params[:url][:facebook].present? || params[:url][:twitter].present?)
      if current_subdomain.update(facebook_url: params[:url][:facebook], twitter_url: params[:url][:twitter])
        Subdomain.update_or_create_milestone(current_subdomain.id, Subdomain.milestone_for_create_media_store)
        flash[:message] = "You have successfully updated your site's promotion URL"
        flash[:type] = :success
      end
    end    
    redirect_to "/my_account#brand_new_promotion"
  end

  ##--
  ## Created On: 27/04/2016
  ## Purpose: request for exchange approval
  def exchange_request_approval
    @subdomains = Subdomain.where(exchange_status: 1).paginate(per_page: 20, :page => params[:page]) 
  end

  ##--
  ## Created On: 02/05/2016
  ## Purpose: To approve or reject the Exchange request for subdomains.
  #++
  def exchange_approval
    subdomain_ids = params[:approve_subdomain]
    if params.include?("approve_exchange_request")
      subdomain = Subdomain.where("id in (?)", subdomain_ids).update_all(exchange_status: 2)
      flash[:message] = "You have successfully approved the request"
      flash[:type] = :success
    else
      subdomain = Subdomain.where("id in (?)", subdomain_ids).update_all(exchange_status: 3)
      flash[:message] = "You have successfully rejected the request"
      flash[:type] = :success
    end
    redirect_to '/exchange_request_approval'
  end

  def exchange_list
    #exchange_name = params[:exchange_list]
    exclude_subdomain_ids = ([current_subdomain.id] + current_subdomain.exchanges.map(&:connection_id) + current_subdomain.inverse_connections.map(&:id)).flatten.compact.uniq
    if params[:search_exchange].present?      
      @subdomains = Subdomain.where("id not in (?) and exchange_status = 2 and subdomain_name like ?", exclude_subdomain_ids,'%'+params[:search_exchange]+'%').paginate(:per_page => 20, :page => params[:page])
      #redirect_to exchange_list_path and return
    else
      @subdomains = Subdomain.where("id not in (?) and exchange_status = 2", exclude_subdomain_ids).paginate(:per_page => 20, :page => params[:page])
    end
    @connections = current_subdomain.connections + current_subdomain.inverse_connections
  end

  def create_exchange
    subdomain_exchange = Exchange.where("subdomain_id =? or connection_id =?",current_subdomain.id,current_subdomain.id).count == 5
    unless subdomain_exchange
      @exchange = current_subdomain.exchanges.build(:connection_id => params[:connection_id])
      if @exchange.save
        Subdomain.update_or_create_milestone(current_subdomain.id, Subdomain.milestone_for_select_top_5) if subdomain_exchange
        flash[:notice] = "Exchange added successfully."
        redirect_to exchange_list_path
      end
    else
      flash[:error] = "You can only add 5 Exchanges at a time."
      redirect_to exchange_list_path
    end
  end

  def destroy_exchange
    @exchange = Exchange.find(params[:connection_id])
    @exchange.destroy
    flash[:notice] = "Exchange removed successfully."
    redirect_to exchange_list_path
  end

  
private
  ##--
  ## Created On: 11/12/2014
  ## Purpose: callback to redirect to proper my account page
  #++
  def redirect_to_myaccount
    redirect_to "/customer_profile" and return true if current_user.is_customer?
    #redirect_to "/get_statistic_overview" and return true if current_user.user_role.role == "SUBSCRIPTION_ADMIN"
    false
  end

  def permit_theme_params
    params.require(:theme).permit(:theme_name, :logo_image_path, :background_color, :background_image_path, :subdomain_title_color, :header_color, :footer_color, :subdomain_id)
  end

  def submerchant_account_params
    params.require(:submerchant_accounts).permit(:first_name,:last_name,:street_address,:locality,:region,:postal_code,:account_number, :routing_number,:date_of_birth)
  end

  ##--
  ## Created On: 09/12/2014
  ## Purpose: To search messages by different columns
  #++
  def media_search(search_by, search_text, last, subdomain, pagination = "1", per_page = 15, order_by = "created_at", order = "DESC")
    @status_code = 200
    @uploaded_medias = []
    last = last.blank? ? "99999" : last
    case search_by
    when 'all'
      @uploaded_medias = UploadedMedium.search_by_all(search_text, last, subdomain).order("#{order_by} #{order}").paginate(per_page: per_page, :page => pagination)
    when 'artist'
      @uploaded_medias = UploadedMedium.search_by_artist(search_text, last, subdomain).order("#{order_by} #{order}").paginate(per_page: per_page, :page => pagination)
    when 'tag'
      @uploaded_medias = UploadedMedium.search_by_tag(search_text, last, subdomain).order("#{order_by} #{order}").paginate(per_page: per_page, :page => pagination)
    when 'file'
      @uploaded_medias = UploadedMedium.search_by_file_name(search_text, last, subdomain).order("#{order_by} #{order}").paginate(per_page: per_page, :page => pagination)
    when 'subcategory'
      @uploaded_medias = UploadedMedium.search_by_subcategory(search_text, last, subdomain).order("#{order_by} #{order}").paginate(per_page: per_page, :page => pagination)
    when 'category'
      if params[:home_search_text].blank?
        @uploaded_medias = subdomain.is_ocm? ? UploadedMedium.where("created_at >= ?", last.to_i.days.ago.to_date).order("#{order_by} #{order}").paginate(per_page: per_page, :page => pagination) : UploadedMedium.select(:media_file_name, :media_amount, :presenter_name, :media_art_file_name, :id).where("subdomain_id = ? AND created_at >= ?", subdomain.id, last.to_i.days.ago.to_date).order("#{order_by} #{order}").paginate(per_page: per_page, :page => params[:page])
      else
        @uploaded_medias = subdomain.is_ocm? ? Category.find(params[:home_search_text]).uploaded_media.where("uploaded_media.created_at >= ?", last.to_i.days.ago).order("#{order_by} #{order}").paginate(per_page: per_page, :page => pagination) : Category.find(params[:home_search_text]).uploaded_media.where("uploaded_media.subdomain_id = ? AND uploaded_media.created_at >= ?", current_subdomain, last.to_i.days.ago).order("#{order_by} #{order}").paginate(per_page: per_page, :page => params[:page])
      end
    else
      @status_code = 403
    end
  end

  def restrict_non_tenant_owner
    if current_user && !current_user.is_tenant_owner?
      flash[:title] = "Access is restricted"
      flash[:message] = "Access for this page is restricted."
      flash[:type] = :error        
      redirect_to home_path 
    end
  end

end
