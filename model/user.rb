class User < ActiveRecord::Base
  attr_accessor :role
  belongs_to :user_role
  has_one :address, dependent: :destroy
  has_many :transactions, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :faqs, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :downloads, dependent: :destroy
  has_one :cart, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :feedbacks, dependent: :destroy
  has_one :subdomain_user, dependent: :destroy
  has_one :subdomain, through: :subdomain_user
  has_many :subscriptions, through: :subdomain_user  
  has_one :picture, as: :imageable, :dependent => :destroy
  belongs_to :title
  has_one :submerchant_accounts
  has_many :submerchant_transactions
  devise :database_authenticatable, :registerable,
         :recoverable, :validatable
  include ActiveModel::Validations
  before_save :set_role, :remove_whitespace

  ##--
  ## Created On: 04/12/2014
  ## Purpose: validations of registration form at model level
  ##
  validates :username, :presence => true, :uniqueness => true, :length => { :in => 2..50 }
  validates :password, :confirmation => true 
  validates_length_of :password, :in => 8..32, :on => :create
  validates :name, :presence => true 
  
  VALID_EMAIL_REGEX = /\A([A-za-z0-9._-]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates :email, 
            :presence => {:message => "Enter your email address!" },
            :format => { :with => VALID_EMAIL_REGEX, :message => "Enter a valid email address !"}
            
  def email_required?
    false
  end

  def email_changed?
    false
  end
  ##--
  ## Created On: 04/12/2014
  ## Purpose: To find the first name of a user
  #++
  def first_name
    name.split(" ").first
  end
  ##--
  ## Created On: 01/12/2014
  ## Purpose: To find list of downloaded media
  #++
  def downloaded_media
    Download.joins(:uploaded_media).where("downloads.user_id = ?", self.id)
  end
  ##--
  ## Created On: 12/12/2014
  ## Purpose: To find list of liked media
  #++
  def liked_media
    Like.joins(:uploaded_media).where("likes.user_id = ?", self.id)
  end
  ##--
  ## Created On: 12/12/2014
  ## Purpose: To find list of rated media
  #++
  def rated_media
    Rating.joins(:uploaded_media).where("ratings.user_id = ?", self.id) 
  end
  ##--
  ## Created On: 29/01/2015
  ## Purpose: To find the subscription admin
  #++
  def is_subscription_admin?
    self.user_role.role == "SUBSCRIPTION_ADMIN"
  end

  def is_content_manager?
    self.user_role.role == "CONTENT_MANAGER"
  end

  def is_tenant_owner?
    self.user_role.role == "TENANT_OWNER"
  end

  def is_super_admin?
    self.user_role.role == "SUPER_ADMIN"
  end

  def is_free_user?
    self.current_subscription == nil
  end

  ##--
  ## Created On: 01/12/2014
  ## Purpose: CallBack, To set role of user by role name
  #++
  def set_role
    if self.role == "CUSTOMER"
      self.user_role_id = UserRole.find_by_role(self.role).id
    elsif SubdomainType.exists?(subdomain_type: self.role)
      self.user_role_id = UserRole.find_by_role("TENANT_OWNER").id
    end unless self.role.blank?
  end

  ##--
  ## Created On: 01/12/2014
  ## Purpose: To check user has permission to upload media
  #++
  def can_upload?
    self.user_role.upload_media
  end

  ##--
  ## Created On: 01/12/2014
  ## Purpose: To check user has permission to delete media
  #++
  def can_delete_media?
    self.user_role.delete_media
  end

  ##--
  ## Created On: 01/12/2014
  ## Purpose: To check user has permission to search user
  #++
  def can_search_user?
    self.user_role.search_user
  end

  ##--
  ## Created On: 01/12/2014
  ## Modified On: 12/12/2014
  ## Purpose: To find current plan of user
  #++
  def current_plan
    return nil if current_subscription.nil?
    current_subscription.plans.where.not(plan_type: PlanType.where("type_name in (?)", ["ADDON","SETUP"])).first
  end

  ##--
  ## Created On: 12/12/2014
  ## Purpose: To find current subscription
  #++
  def current_subscription
    self.subdomain_user.subscriptions.order(:created_at).last rescue return nil
  end

  ##--
  ## Created On: 01/12/2014
  ## Purpose: To check, user is customer?
  #++
  def is_customer?
    self.user_role.role == "CUSTOMER"
  end

  def is_ocm?
    self.user_role.role == "OCM_ADMIN"
  end

  ##--
  ## Created On: 01/12/2014
  ## Purpose: To check, user is subdomain_admin?
  #++
  def is_subdomain_admin?
    self.user_role.role != "CUSTOMER"
  end

  ##--
  ## Created On: 12/12/2014
  ## Purpose: To subscribe the user for given plan
  #++
  def subscribe_to(plan_name)
    self.subscriptions.create(effective_from: DateTime.now, effective_to: 30.days.from_now, is_recurring: false).plan_subscriptions.create!(plan: Plan.find_by_plan_name(plan_name))
  end
  
  ##--
  ## Created On: 18/12/2014
  ## Purpose: To check user has permission to change site style
  #++
  def can_promote_site
    self.user_role.change_site_style
  end

  ##--
  ## Created On: 18/12/2014
  ## Purpose: To check user has permission see the statistics
  #++
  def can_see_statistics
    self.user_role.manage_payments && self.user_role.recurring_subscriptions
  end

  ##--
  ## Created On: 18/12/2014
  ## Purpose: To check user has permission to change site style
  #++
  def can_allocate_role?
    self.user_role.allocate_role
  end

  
  ##--
  ## Created On: 21/12/2014
  ## Purpose: To deactivate subdomain
  #++ 
  def deactivate!
    self.deactive = true
    save
  end

  ##--
  ## Created On: 20/12/2014
  ## Purpose: To find the admin users of current subdomain.
  #++
  def self.admin_users_of_subdomain(current_subdomain)
    User.all.collect{|user| user if ((user.user_role.role != 'CUSTOMER') && (user.user_role.role != 'TENANT_OWNER') && (user.subdomain.subdomain_name == current_subdomain)) }
  end

  def remove_whitespace
    self.name = self.name.split.join(" ")
  end
  
  ##--
  ## Created On: 02/01/2015
  ## Modified on: 21/01/2015
  ## Purpose: To deactivate users with expired subscriptions and notify users with unpaid invoices.
  #++
  def self.expired_subscription
    grace_period = Subscription.get_grace_period
    expired_subscriptions_subdomain_user_ids = Subscription.group("subdomain_user_id desc").collect{|subscription| subscription.subdomain_user_id if (subscription.effective_to < grace_period.days.ago.beginning_of_day)}.compact
    unpaid_invoices_subdomain_user_ids = Subscription.group("subdomain_user_id desc").collect{|subscription| subscription.subdomain_user_id if (subscription.effective_to >= grace_period.days.ago.beginning_of_day) && (subscription.effective_to < DateTime.now)}.compact
    user_ids = SubdomainUser.where("id in (?)", expired_subscriptions_subdomain_user_ids).map(&:user_id)
    self.where("id in (?) and deactive is NOT true",user_ids).each do |user|
        user.update(deactive: true)
    end
    unpaid_invoices_users = SubdomainUser.where("id in (?)", unpaid_invoices_subdomain_user_ids).map(&:user_id)
    unpaid_invoices_users.each do |user|
      notify_user = where("id = ?", user).first
      effective_to = Subscription.where("subdomain_user_id in (?)", (SubdomainUser.where("user_id = ?", user).map(&:id))).last.effective_to.to_date
      UserMailer.notification_for_expired_subscription(notify_user, effective_to, grace_period).deliver
    end
  end

  ##--
  ## Created On: 12/01/2016
  ## Purpose: To send mails (10,7,3,1 days before expiry of account) to active users notifying their account is going to expire soon.
  #++
  def self.notifcation_mails_before_expiration_of_account
    grace_periods = Subscription.get_mails_grace_period
    grace_periods.each do |grace_period|
      grace_period = grace_period.to_i
      subscriptions_to_be_expired_sumdomain_user_ids = Subscription.group("subdomain_user_id desc").collect {|subscription| subscription.subdomain_user_id if (subscription.effective_to == grace_period.days.from_now.beginning_of_day)}.compact
      collect_ids_and_send_mail(subscriptions_to_be_expired_sumdomain_user_ids, 'before', grace_period)
    end
  end

  ##--
  ## Created On: 12/01/2016
  ## Purpose: To send mails (1,3,7 days after expiry of account) to active users notifying their account is going to expire after grace period.
  #++
  def self.notifcation_mails_after_expiration_of_account
    grace_periods = Subscription.grace_value_for_notification_after_expiry
    grace_periods.each do |grace_period|
      grace_period = grace_period.to_i
      subscriptions_to_be_expired_sumdomain_user_ids = Subscription.group("subdomain_user_id desc").collect {|subscription| subscription.subdomain_user_id if (subscription.effective_to == grace_period.days.ago.beginning_of_day)}.compact      
      collect_ids_and_send_mail(subscriptions_to_be_expired_sumdomain_user_ids, 'after', grace_period)
    end
  end
  
  def self.collect_ids_and_send_mail(subscriptions_to_be_expired_sumdomain_user_ids, on, grace_period)
    user_ids = SubdomainUser.where("id in (?)", subscriptions_to_be_expired_sumdomain_user_ids).map(&:user_id)
    unless user_ids.blank?
      user_ids.each do |user_id|        
        user = where("id = ?", user_id ).first
        effective_to = Subscription.where("subdomain_user_id in (?)",(SubdomainUser.where("user_id = ?", user_id).map(&:id))).last.effective_to.to_date
        decide_mailer(on, user, effective_to, grace_period)        
      end
    end
  end

  def self.decide_mailer(on, user, effective_to, grace_period)
    if (on == "after")
      UserMailer.notification_for_user_after_expiry(user, effective_to, grace_period).deliver
    elsif (on == "before")
      UserMailer.notification_for_user_to_be_expired(user, effective_to, grace_period).deliver
    end
  end

end