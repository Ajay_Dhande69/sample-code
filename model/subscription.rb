class Subscription < ActiveRecord::Base
  belongs_to :subdomain_user
  has_many :plan_subscriptions, dependent: :destroy
  has_many :plans, through: :plan_subscriptions
  has_many :show_transaction, class_name: "Transaction", foreign_key: "subscription_id", dependent: :destroy

  ##--
  ## Created On: 12/12/2014
  ## Purpose: To check, Is subscription expire?
  #++
  def is_expired?
    self.effective_to <= DateTime.now
  end

  def self.get_grace_period
    grace = YAML.load_file("#{Rails.root}/config/grace_period.yml")
    grace["grace_value"].to_i
  end

  def self.get_mails_grace_period
    grace = YAML.load_file("#{Rails.root}/config/grace_period.yml")
    grace["grace_value_for_notification"]
  end

  def self.grace_value_for_notification_after_expiry
    grace = YAML.load_file("#{Rails.root}/config/grace_period.yml")
    grace["grace_value_for_notification_after_expiry"]
  end

  ##--
  ## Created on: 24/12/2014
  ## Modified on: 20/01/2015
  ## Re-written on: 07/04/2015
  ## Purpose: To find the active subscriptions of a subdomain.
  ##++
  def self.active_subscriptions(subdomain)
    active_subscription_users = subdomain.users.where(user_role_id: 5, deactive: [false, nil], cancelled_on: nil).order(:username).collect do |user|
      subscription = user.current_subscription
      if subscription
        subscription.effective_to > DateTime.now ? user : nil
      else
        user
      end
    end
    active_subscription_users.compact
  end

  ##--
  ## Created on: 26/12/2014
  ## Modified on: 20/01/2015
  ## Purpose: To find unpaid invoices pending since last 14 days for subdomain.
  ##++
  def self.unpaid_invoices(current_subdomain_id, current_user_id)
    setup_plan_ids = Plan.where("plan_type_id in (?)", PlanType.where("type_name like '%SETUP%'").map(&:id)).map(&:id)
    customer_user_ids = SubdomainUser.where("subdomain_id = ?", current_subdomain_id).map(&:id).drop(1)
    last_subscription_ids = Subscription.group("subdomain_user_id desc")
    customer_last_subscriptions = last_subscription_ids.collect{|subscription| subscription.id if (customer_user_ids.include? subscription.subdomain_user_id) && (subscription.effective_to > get_grace_period.days.ago.beginning_of_day) && (subscription.effective_to < DateTime.now)}.compact
    plan_ids = PlanSubscription.where("subscription_id in (?) and plan_id not in (?)", customer_last_subscriptions, setup_plan_ids).map(&:plan_id)
    plan_ids.collect{|plan_id| Plan.find(plan_id).plan_amount rescue 0.0}.sum 
  end

  ##--
  ## Created on: 26/12/2014
  ## Modified on: 20/01/2015
  ## Purpose: To find unpaid invoices pending since last 14 days for ocm.
  ##++
  def self.unpaid_invoices_of_ocm
    setup_plan_ids = Plan.where("plan_type_id in (?)", PlanType.where("type_name like '%SETUP%'").map(&:id)).map(&:id)
    last_subscription_ids = self.group("subdomain_user_id desc").collect{|subscription| subscription.id if subscription.effective_to > get_grace_period.days.ago.beginning_of_day && subscription.effective_to < DateTime.now}.compact
    plan_ids = PlanSubscription.where("subscription_id in (?) and plan_id not in (?)", last_subscription_ids, setup_plan_ids).collect{|e| e.plan_id}
    plan_ids.collect{|plan_id| Plan.find(plan_id).plan_amount rescue 0.0}.sum  
  end

  def self.active_subscriptions_of_ocm
    active_users = User.where("user_role_id in (?) and deactive is NOT true and cancelled_on is null",UserRole.where("role in (?)", ["TENANT_OWNER", "CUSTOMER"]).map(&:id))
    active_subdomain_user_ids = SubdomainUser.where("user_id in (?)", active_users.map(&:id)).map(&:id)
    free_users = active_users.collect{|user| user.id if user.current_subscription == nil}.compact.length    
    self.group("subdomain_user_id desc").collect{|subscription| subscription.id if (active_subdomain_user_ids.include? subscription.subdomain_user_id) && (subscription.effective_to > DateTime.now)}.compact.length + free_users
  end
end
